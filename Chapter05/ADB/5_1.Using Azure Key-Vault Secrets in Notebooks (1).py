# Databricks notebook source
#Storage account key is stored in Azure Key-Vault as a sceret. The secret name is blobstoragesecret and KeyVaultScope is the name of the scope we have created. We can also store the storage account name as a new secret if we don't want users to know the name of the storage account.

storageAccount="cookbookblobstorage1"
storageKey = dbutils.secrets.get(scope="KeyVaultScope",key="blobstoragesecret")
mountpoint = "/mnt/KeyVaultBlob"
storageEndpoint =   "wasbs://rawdata@{}.blob.core.windows.net".format(storageAccount)
storageConnSting = "fs.azure.account.key.{}.blob.core.windows.net".format(storageAccount)

try:
  dbutils.fs.mount(
  source = storageEndpoint,
  mount_point = mountpoint,
  extra_configs = {storageConnSting:storageKey})
except:
    print("Already mounted...."+mountpoint)


# COMMAND ----------

# MAGIC %fs ls /mnt/KeyVaultBlob

# COMMAND ----------

display(dbutils.fs.ls("/mnt/KeyVaultBlob/Customer/parquetFiles"))

# COMMAND ----------

#Lets read data from csv file which is copied to Blob Storage
df_cust= spark.read.format("parquet").option("header",True).load("dbfs:/mnt/KeyVaultBlob/Customer/parquetFiles/*.parquet")

# COMMAND ----------

display(df_cust.limit(10))

# COMMAND ----------

df_cust.count()

# COMMAND ----------


