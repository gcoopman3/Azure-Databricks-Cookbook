# Databricks notebook source
from pyspark.sql.types import *
from pyspark.sql.functions import col
from pyspark.sql.functions import col,sum,avg,max

# COMMAND ----------

# Reading customer csv files in a dataframe
df_cust= spark.read.format("csv").option("header",True).option("inferSchema", True).load("dbfs:/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

display(df_cust.limit(10))

# COMMAND ----------

#Group By on C_MKTSEGMENT.
df_cust_agg = df_cust.groupBy("C_MKTSEGMENT")\
   .agg(sum("C_ACCTBAL").cast('decimal(20,3)').alias("sum_acctbal"), \
     avg("C_ACCTBAL").alias("avg_acctbal"), \
     max("C_ACCTBAL").alias("max_bonus")).orderBy("avg_acctbal",ascending=False)

# COMMAND ----------

display(df_cust_agg)

# COMMAND ----------

#Without groupBy and check the DAG for this query execution by executing the display command in the next cell
df_cust_agg = df_cust.\
agg(sum("C_ACCTBAL").cast('decimal(20,3)').alias("sum_acctbal"), \
     avg("C_ACCTBAL").alias("avg_acctbal"), \
     max("C_ACCTBAL").alias("max_bonus"))#.orderBy("avg_acctbal",ascending=False)

# COMMAND ----------

display(df_cust_agg)

# COMMAND ----------


