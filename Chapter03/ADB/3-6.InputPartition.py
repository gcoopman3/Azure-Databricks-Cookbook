# Databricks notebook source
display(dbutils.fs.ls("/mnt/Gen2/Customer/csvFiles"))

# COMMAND ----------

df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

print(spark.conf.get("spark.sql.files.maxPartitionBytes"))

# COMMAND ----------

df.rdd.getNumPartitions()

# COMMAND ----------

spark.conf.set("spark.sql.files.maxPartitionBytes", "134217728") # 128 MB

# COMMAND ----------

spark.conf.set("spark.sql.files.maxPartitionBytes", "1048576") # 16 MB

# COMMAND ----------

df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/Customer/csvFiles")
df.rdd.getNumPartitions()

# COMMAND ----------


