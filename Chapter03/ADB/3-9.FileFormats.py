# Databricks notebook source
df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

display(df)

# COMMAND ----------

sortDF = df.sort('C_CUSTKEY')
display(sortDF)

# COMMAND ----------

sortDF.write.mode("overwrite").format("parquet").option("path", "/tmp/customer/sortedData1/").saveAsTable("sortedData")
df.write.mode("overwrite").format("parquet").option("path", "/tmp/customer/unsortedData1").saveAsTable("unsortedData")
df.write.mode("overwrite").format("json").option("path", "/tmp/customer/jsonData1").saveAsTable("jsonData")

# COMMAND ----------

display(dbutils.fs.ls("/tmp/customer/sortedData1/"))

# COMMAND ----------

display(dbutils.fs.ls("/tmp/customer/unsortedData1/"))


# COMMAND ----------

display(dbutils.fs.ls("/tmp/customer/jsonData1/"))
