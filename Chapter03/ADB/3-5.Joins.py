# Databricks notebook source
from pyspark.sql.types import *
from pyspark.sql.functions import col
from pyspark.sql.functions import col,sum,avg,max

# COMMAND ----------

#Creating the schema for the Orders csv files
order_schema = StructType([
    StructField("O_ORDERKEY", IntegerType()),
    StructField("O_CUSTKEY", StringType()),
    StructField("O_ORDERSTATUS", StringType()),
    StructField("O_TOTALPRICE", DoubleType()),
    StructField("O_ORDERDATE", DateType()),
    StructField("O_ORDERPRIORITY", StringType()),
    StructField("O_CLERK", StringType()),
    StructField("O_SHIPPRIORITY", IntegerType()),
    StructField("O_COMMENT", StringType())
])


# COMMAND ----------

#Creating the schema for the Customer csv files
cust_schema = StructType([
    StructField("C_CUSTKEY", IntegerType()),
    StructField("C_NAME", StringType()),
    StructField("C_ADDRESS", StringType()),
    StructField("C_NATIONKEY", ShortType()),
    StructField("C_PHONE", StringType()),
    StructField("C_ACCTBAL", DoubleType()),
    StructField("C_MKTSEGMENT", StringType()),
    StructField("C_COMMENT", StringType())
])


# COMMAND ----------

#Creating the Orders dataframe by specifying the schema 
df_ord_sch= spark.read.format("csv").option("header",True).schema(order_schema).load("/mnt/Gen2/Orders/csvFiles/")

# COMMAND ----------

#Creating the dataframe by specifying the schema using .schema option. We don't see any DAG getting created with schema is specified
df_cust_sch= spark.read.format("csv").option("header",True).schema(cust_schema).load("/mnt/Gen2/Customer/csvFiles/")

# COMMAND ----------

# check for parameter value and the default value is 10 MB
spark.conf.get("spark.sql.autoBroadcastJoinThreshold")

# COMMAND ----------

#Setting to 2 MB
spark.conf.set("spark.sql.autoBroadcastJoinThreshold",2194304)

# COMMAND ----------

# Check the execution plan and you will find sort merge join being used 
df_ord_sch.join(df_cust_sch, df_ord_sch.O_CUSTKEY == df_cust_sch.C_CUSTKEY, "inner").count()

# COMMAND ----------

df_ord_sch.join(df_cust_sch.hint("broadcast"), df_ord_sch.O_CUSTKEY == df_cust_sch.C_CUSTKEY, "inner").count()

# COMMAND ----------

df_ord_sch.join(df_cust_sch.hint("merge"), df_ord_sch.O_CUSTKEY == df_cust_sch.C_CUSTKEY, "inner").count()

# COMMAND ----------


