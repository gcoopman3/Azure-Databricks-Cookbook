# Databricks notebook source
df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

# Check the duration of execution with default 200 partitions 
spark.conf.set("spark.sql.shuffle.partitions", 200)
mktSegmentDF = df.groupBy("C_MKTSEGMENT").count().collect()

# COMMAND ----------

# Check the duration of execution with default 30 partitions 
spark.conf.set("spark.sql.shuffle.partitions", 30)
mktSegmentDF = df.groupBy("C_MKTSEGMENT").count().collect()
