# Databricks notebook source
from pyspark.sql.types import *
from pyspark.sql.functions import col
from pyspark.sql.functions import col,sum,avg,max

# COMMAND ----------

#Creating the schema for the Customer csv files
cust_schema = StructType([
    StructField("C_CUSTKEY", IntegerType()),
    StructField("C_NAME", StringType()),
    StructField("C_ADDRESS", StringType()),
    StructField("C_NATIONKEY", ShortType()),
    StructField("C_PHONE", StringType()),
    StructField("C_ACCTBAL", DoubleType()),
    StructField("C_MKTSEGMENT", StringType()),
    StructField("C_COMMENT", StringType())
])


# COMMAND ----------

df_cust_sch= spark.read.format("csv").option("header",True).schema(cust_schema).load("/mnt/Gen2/Customer/csvFiles/")

# COMMAND ----------

display(df_cust_sch.limit(10))

# COMMAND ----------

df_agg = df_cust_sch.groupBy("C_MKTSEGMENT").agg(avg("C_ACCTBAL").alias("AvgAcctBal"))

# COMMAND ----------

df_agg.show()

# COMMAND ----------

df_agg.where(df_agg.C_MKTSEGMENT=="MACHINERY").where(df_agg.AvgAcctBal>4500).show()

# COMMAND ----------

df_cust_sch.createOrReplaceTempView("df_Customer")

# COMMAND ----------

# Note the plans are same using DF API or SQL API
sql = spark.sql("SELECT C_MKTSEGMENT, count(1) FROM df_Customer GROUP BY C_MKTSEGMENT ")

dataframe = df_cust_sch\
  .groupBy("C_MKTSEGMENT")\
  .count()

sql.explain()
dataframe.explain()

# COMMAND ----------


