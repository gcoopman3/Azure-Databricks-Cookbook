# Databricks notebook source
df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/Customer/csvFiles/")

# COMMAND ----------

spark.conf.set("spark.sql.files.maxPartitionBytes", "134217728") # 128 MB
df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/Customer/csvFiles/")
print(f"Number of partitions = {df.rdd.getNumPartitions()}")
df.write.mode("overwrite").option("path", "dbfs:/tmp/datasources/customerSingleFile/").saveAsTable("customerSingleFile")

# COMMAND ----------

repartitionedDF = df.repartition(8)
print('Number of partitions: {}'.format(repartitionedDF.rdd.getNumPartitions()))

# COMMAND ----------

repartitionedDF = df.repartition('C_MKTSEGMENT')
df = spark.read.format("csv").option("header","true").load("/mnt/Gen2/")
print('Number of partitions: {}'.format(repartitionedDF.rdd.getNumPartitions()))

# COMMAND ----------

coalescedDF = df.coalesce(2)
print('Number of partitions: {}'.format(coalescedDF.rdd.getNumPartitions()))

# COMMAND ----------

df.write.option("maxRecordsPerFile", 1000).mode("overwrite").partitionBy("C_MKTSEGMENT").option("path", "dbfs:/tmp/datasources/customer/parquet/maxfiles/").saveAsTable("customer_maxfiles")

%fs ls dbfs:/tmp/datasources/customer/parquet/maxfiles/C_MKTSEGMENT=BUILDING/
  
df_read = spark.read.format("parquet").option("inferSchema","True").load("dbfs:/tmp/datasources/customer/parquet/maxfiles/C_MKTSEGMENT=BUILDING/part-00000-tid-2745561518541164111-d6e0dd05-db1c-4716-b1ae-604c8817099a-177-302.c000.snappy.parquet")
df_read.count()

# COMMAND ----------

# MAGIC %fs ls dbfs:/tmp/datasources/customer/parquet/maxfiles/C_MKTSEGMENT=BUILDING/

# COMMAND ----------

df_read = spark.read.format("parquet").option("inferSchema","True").load("dbfs:/tmp/datasources/customer/parquet/maxfiles/C_MKTSEGMENT=BUILDING/part-00000-tid-2745561518541164111-d6e0dd05-db1c-4716-b1ae-604c8817099a-177-302.c000.snappy.parquet")
df_read.count()

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from customerSingleFile limit 10

# COMMAND ----------

# MAGIC %fs ls dbfs:/tmp/datasources/customerSingleFile
