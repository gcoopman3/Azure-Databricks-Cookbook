# Databricks notebook source
from pyspark.sql.types import *
from pyspark.sql.functions import col
from pyspark.sql.functions import col,sum,avg,max

# COMMAND ----------

dbutils.fs.ls("dbfs:/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

# Reading customer csf files in a dataframe with specifying the schema explicitly
df_cust= spark.read.format("csv").option("header",True).option("inferSchema", True).load("dbfs:/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

# Getting the number of partitions
print(f" Number of partitions with all files read = {df_cust.rdd.getNumPartitions()}")

# COMMAND ----------

#Creating the schema for the Customer csv files
cust_schema = StructType([
    StructField("C_CUSTKEY", IntegerType()),
    StructField("C_NAME", StringType()),
    StructField("C_ADDRESS", StringType()),
    StructField("C_NATIONKEY", ShortType()),
    StructField("C_PHONE", StringType()),
    StructField("C_ACCTBAL", DoubleType()),
    StructField("C_MKTSEGMENT", StringType()),
    StructField("C_COMMENT", StringType())
])


# COMMAND ----------

#Creating the dataframe by specifying the schema using .schem() option. We don;t see any DAG getting created with schema is specified
df_cust_sch= spark.read.format("csv").option("header",True).schema(cust_schema).load("/mnt/Gen2/Customer/csvFiles/")

# COMMAND ----------

# Using first file 
df_cust_sch= spark.read.format("csv").option("header",True).load("/mnt/Gen2/Customer/csvFiles/part-00000-tid-3200334632332214470-9b4dec79-7e2e-495d-8657-3b5457ed3753-108-1-c000.csv")

# COMMAND ----------

#Getting the number of partitions by specifying only one csv file.
print(f" Number of partitions with 1 file read  = {df_cust_sch.rdd.getNumPartitions()}")

# COMMAND ----------


