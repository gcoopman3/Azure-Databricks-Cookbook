# Databricks notebook source
import matplotlib.pyplot as plt

# COMMAND ----------



# COMMAND ----------

import pandas as pd
dataframe =  pd.DataFrame({"Stock":[300, 345, 330, 320, 600, 650, 670, 680, 690, None, 650, 650, 640, 660]})

# COMMAND ----------

dataframe

# COMMAND ----------

dataframe.plot()

# COMMAND ----------

# MAGIC %md
# MAGIC # DROP MISSING VALUE

# COMMAND ----------

missingdata = dataframe.copy()
missingdata.dropna(inplace=True)

# COMMAND ----------

missingdata

# COMMAND ----------

missingdata.plot()

# COMMAND ----------

# MAGIC %md
# MAGIC # IMPUTE with MEAN

# COMMAND ----------

meandf = dataframe.copy()

# COMMAND ----------

meandf.fillna(meandf.mean(), inplace=True)

# COMMAND ----------

meandf

# COMMAND ----------

meandf.plot()

# COMMAND ----------

from numpy import nan, isnan
from sklearn.impute import SimpleImputer

# COMMAND ----------

imputer = SimpleImputer(missing_values=nan, strategy='mean')

# COMMAND ----------

imputedata = dataframe.copy()

# COMMAND ----------

imputedata

# COMMAND ----------

values = imputedata.values
transformed_values = imputer.fit_transform(values)

# COMMAND ----------

plt.plot(transformed_values)
plt.plot(missingdata['Stock'])
plt.show()

# COMMAND ----------

# MAGIC %md
# MAGIC # Forward Fill and Backward Fill

# COMMAND ----------

dataframe

# COMMAND ----------

fdata = dataframe['Stock'].ffill(axis = 0)
fdata

# COMMAND ----------

bdata = dataframe['Stock'].bfill(axis = 0)
bdata

# COMMAND ----------

plt.plot(transformed_values, label ="StandardImpute")
plt.plot(missingdata['Stock'], label = "Drop")
plt.plot(fdata , label = "Forward Fill")
plt.plot(bdata, label = "Backward Fill")
plt.legend()
plt.show()

# COMMAND ----------

dataframe

# COMMAND ----------

import numpy as np

# COMMAND ----------

Q1 = np.percentile(bdata, 25, interpolation='midpoint')
Q1

# COMMAND ----------

Q1

# COMMAND ----------

from scipy import stats

# COMMAND ----------

stats.iqr(dataframe)

# COMMAND ----------

l1 = dataframe['Stock'].tolist()

# COMMAND ----------

stats.iqr(bdata)

# COMMAND ----------

def findlimits(data):
    Q1 =  np.percentile(data, 25, interpolation='midpoint')
    Q3 =  np.percentile(data, 75, interpolation='midpoint')
    IQR= Q3 - Q1
    UB = Q3 + 1.5*(IQR)
    LB = Q1 - 1.5*(IQR)
    return UB, LB

# COMMAND ----------

Upper, lower = findlimits(bdata)

# COMMAND ----------

Upper

# COMMAND ----------

lower

# COMMAND ----------

bdata

# COMMAND ----------


