# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

#  We are reading from kafkaenabledhub1. Please change this value if you are using a different EventHub name in the Python script.
TOPIC = "kafkaenabledhub1"
BOOTSTRAP_SERVERS = "kafkaenabledeventhubns.servicebus.windows.net:9093"
EH_SASL = "kafkashaded.org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://kafkaenabledeventhubns.servicebus.windows.net/;SharedAccessKeyName=sendreceivekafka;SharedAccessKey=FLdyRBpyGt6Pluis5b79vRTwuHOy/OjwijF7jsCmFnA=\";"
GROUP_ID = "$Default"

# // Read stream using Spark SQL (structured streaming)
# // consider adding .option("startingOffsets", "earliest") to read earliest available offset during testing
kafkaDF = spark.readStream \
    .format("kafka") \
    .option("subscribe", TOPIC) \
    .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
    .option("kafka.sasl.mechanism", "PLAIN") \
    .option("kafka.security.protocol", "SASL_SSL") \
    .option("kafka.sasl.jaas.config", EH_SASL) \
    .option("kafka.request.timeout.ms", "60000") \
    .option("kafka.session.timeout.ms", "60000") \
    .option("kafka.group.id", GROUP_ID) \
    .option("failOnDataLoss", "false") \
    .option("startingOffsets", "latest") \
    .load()




# COMMAND ----------

#Converting binary datatype to string for the dataframe columns. Without this you cannot use from_json function as it expects the column datatype as string not binary
kafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

# COMMAND ----------

#Adding new column vehiclejson which is a struct and has 5 columns id, timestamp,rpm,speed and kms
newkafkaDF=kafkaDF.withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))
kafkajsonDF=newkafkaDF.select("key","value", "vehiclejson.*")

# COMMAND ----------

#you can uncomment and run the below command to view the column values
# display(kafkajsonDF)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Tumbling window non-overlapping event time

# COMMAND ----------

kafkajsonDF.groupBy(window('timestamp',"1 minutes"),'id').count().orderBy('window') \
.writeStream.format("delta") \
.outputMode("complete") \
.option("truncate", "false") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Agg_Chkpoint_Tumbling1/") \
.option("mergeSchema", "true") \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta_Agg_Tumbling1") 

# COMMAND ----------

# MAGIC %sql
# MAGIC DROP TABLE IF EXISTS VehiclechkpointKafkaEventHub_Delta_Agg_Tumbling;
# MAGIC CREATE TABLE IF NOT EXISTS VehiclechkpointKafkaEventHub_Delta_Agg_Tumbling
# MAGIC USING DELTA
# MAGIC LOCATION "dbfs:/VehiclechkpointKafkaEventHub_Delta_Agg_Tumbling1/"

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT * FROM VehiclechkpointKafkaEventHub_Delta_Agg_Tumbling ORDER BY Window desc

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT * FROM VehiclechkpointKafkaEventHub_Delta_Agg_Tumbling where id='c7bb7aa9-cc1c-453f-a86e-ca171c710e85'

# COMMAND ----------

# MAGIC %md
# MAGIC ### Overlapping windows time with sliding time

# COMMAND ----------

for s in spark.streams.active:
    s.stop()

# COMMAND ----------

kafkajsonDF.groupBy(window('timestamp',"2 minutes","1 minutes"),'id').count().orderBy('window') \
.writeStream.format("delta") \
.outputMode("complete") \
.option("truncate", "false") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Agg_Chkpoint_Overlapping5/") \
.option("mergeSchema", "true") \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping5") 


# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC DROP  TABLE IF EXISTS VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping;
# MAGIC CREATE TABLE IF NOT EXISTS VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC USING DELTA
# MAGIC LOCATION "dbfs:/VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping5/"

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC order by window desc

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC order by window desc

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC where id ='c2c7cb35-2f97-4fab-ab23-62fe24eca7af' 
# MAGIC order by window desc

# COMMAND ----------

# MAGIC %md
# MAGIC ##### After Inserting records for the time frame 2021-07-30T03:08:30.000+0000
# MAGIC 
# MAGIC c2c7cb35-2f97-4fab-ab23-62fe24eca7af

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC where id ='c2c7cb35-2f97-4fab-ab23-62fe24eca7af' 
# MAGIC order by window desc

# COMMAND ----------


