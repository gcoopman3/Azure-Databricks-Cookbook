# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F

# COMMAND ----------

#Storage account and key you will get it from the portal as shown in the Cookbook Recipe.We are mounting Blob storage account and not Gen-2.
storageAccount="cookbookblobstorage1"
storageKey ="xxx-xxxxx-xxxxxx"
mountpoint = "/mnt/Blob"
storageEndpoint =   "wasbs://rawdata@{}.blob.core.windows.net".format(storageAccount)
storageConnSting = "fs.azure.account.key.{}.blob.core.windows.net".format(storageAccount)

try:
  dbutils.fs.mount(
  source = storageEndpoint,
  mount_point = mountpoint,
  extra_configs = {storageConnSting:storageKey})
except:
    print("Already mounted...."+mountpoint)

# COMMAND ----------

# dbutils.fs.unmount("/mnt/Blob")

# COMMAND ----------

# MAGIC %fs ls /mnt/Blob

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

#Reading from EventHub "kafkaenabledhub2"
TOPIC = "kafkaenabledhub2"
BOOTSTRAP_SERVERS = "kafkaenabledeventhubns.servicebus.windows.net:9093"
EH_SASL = "kafkashaded.org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://kafkaenabledeventhubns.servicebus.windows.net/;SharedAccessKeyName=sendreceivekafka;SharedAccessKey=zzzzzzzzzzz\";"
GROUP_ID = "$Default"

# // Read stream using Spark SQL (structured streaming)
# // consider adding .option("startingOffsets", "earliest") to read earliest available offset during testing
kafkaDF = spark.readStream \
    .format("kafka") \
    .option("subscribe", TOPIC) \
    .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
    .option("kafka.sasl.mechanism", "PLAIN") \
    .option("kafka.security.protocol", "SASL_SSL") \
    .option("kafka.sasl.jaas.config", EH_SASL) \
    .option("kafka.request.timeout.ms", "60000") \
    .option("kafka.session.timeout.ms", "60000") \
    .option("kafka.group.id", GROUP_ID) \
    .option("failOnDataLoss", "false") \
    .option("startingOffsets", "latest") \
    .load()




# COMMAND ----------

#Converting binary datatype to string for the dataframe columns. Without this you cannot use from_json function as it expects the column datatype as string not binary
kafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

# COMMAND ----------

#Adding new column vehiclejson which is a struct and has 5 columns id, timestamp,rpm,speed and kms
newkafkaDF=kafkaDF.withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))
kafkajsonDF=newkafkaDF.select("key","value", "vehiclejson.*")

# COMMAND ----------

#you can uncomment and run the below command to view the column values
# display(kafkajsonDF)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Tumbling window non-overlapping event time

# COMMAND ----------

kafkajsonDF.withWatermark("timestamp","4 minutes").groupBy(window('timestamp',"1 minutes"),'id').count().coalesce(1) \
.writeStream.format("delta") \
.outputMode("complete") \
.option("truncate", "false") \
.option("checkpointLocation", "/mnt/Blob/Vehicle_Chkpoint1/") \
.start("/mnt/Blob/Vehicle_Agg") 

# COMMAND ----------

# MAGIC %sql
# MAGIC DROP TABLE IF  EXISTS Vehicle_Agg

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TABLE IF NOT EXISTS Vehicle_Agg
# MAGIC USING delta
# MAGIC LOCATION "/mnt/Blob/Vehicle_Agg/"

# COMMAND ----------

# MAGIC %fs ls /mnt/Blob/Vehicle_Agg/

# COMMAND ----------

# MAGIC %sql
# MAGIC -- truncate table Delta_Agg_Tumbling
# MAGIC SELECT * FROM Vehicle_Agg ORDER BY Window desc

# COMMAND ----------

# MAGIC %md
# MAGIC ### Overlapping windows time with sliding time

# COMMAND ----------

kafkajsonDF.groupBy(window('timestamp',"2 minutes","1 minutes"),'id').count().orderBy('window') \
.writeStream.format("delta") \
.outputMode("complete") \
.option("truncate", "false") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Agg_Chkpoint_Overlapping5/") \
.option("mergeSchema", "true") \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping5") 


# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC drop  table if exists VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping;
# MAGIC CREATE TABLE IF NOT EXISTS VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC USING DELTA
# MAGIC LOCATION "dbfs:/VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping5/"

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VehiclechkpointKafkaEventHub_Delta_Agg_Overlapping
# MAGIC -- where id ='7e0d39fd-7251-483c-92d6-7d6bb5cc164e' 
# MAGIC order by window desc

# COMMAND ----------


