# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

# We can use to this to reset the offset from where we want to start reading data from kafak provided data in that offset is available in Kafka Source
offset = '''
  {
  "VehicleDetails":{"0": 1}
  }
'''

print(offset)

# COMMAND ----------

TOPIC = "kafkaenabledhub"
BOOTSTRAP_SERVERS = "kafkaenabledeventhubns.servicebus.windows.net:9093"
EH_SASL = "kafkashaded.org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://kafkaenabledeventhubns.servicebus.windows.net/;SharedAccessKeyName=sendreceivekafka;SharedAccessKey=4vxbVwasdasdsdasd4aVcUWBvYp44sdasaasasasasasasvoVE=\";"
GROUP_ID = "$Default"

# // Read stream using Spark SQL (structured streaming)
# // consider adding .option("startingOffsets", "earliest") to read earliest available offset during testing
kafkaDF = spark.readStream \
    .format("kafka") \
    .option("subscribe", TOPIC) \
    .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
    .option("kafka.sasl.mechanism", "PLAIN") \
    .option("kafka.security.protocol", "SASL_SSL") \
    .option("kafka.sasl.jaas.config", EH_SASL) \
    .option("kafka.request.timeout.ms", "60000") \
    .option("kafka.session.timeout.ms", "60000") \
    .option("kafka.group.id", "POC") \
    .option("failOnDataLoss", "false") \
    .option("startingOffsets", "latest") \
    .load()




# COMMAND ----------

#Checking if streaming is on and getting the schema for the kakfa dataframe 
print( kafkaDF.isStreaming)
print( kafkaDF.printSchema())

# COMMAND ----------

# display(kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)"))

# COMMAND ----------

#Converting binary datatype to string for the dataframe columns. Without this you cannot use from_json function as it expects the column datatype as string not binary
newkafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

# COMMAND ----------

#Adding new column vehiclejson which is a struct and has 5 columns id, timestamp,rpm,speed and kms
newkafkaDF=newkafkaDF.withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))

# COMMAND ----------

kafkajsonDF=newkafkaDF.select("key","value", "vehiclejson.*")

# COMMAND ----------

#you can uncomment and run the below command to view the column values
# display(kafkajsonDF)

# COMMAND ----------

#Writing the streaming data to Delta tables
#Location for Delta table is dbfs:/Vehiclechkpoint_Delta. We are using default mount point which is available in Databricks cluster. You can use your own mount point as well. Recommended is to mount external ADLS Gen-2 file system 
kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("delta") \
.outputMode("append") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Demo/") \
.option("mergeSchema", "true") \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta") 

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC CREATE TABLE IF NOT EXISTS VehicleDetails_KafkaEnabledEventHub_Delta
# MAGIC USING DELTA
# MAGIC LOCATION "dbfs:/VehiclechkpointKafkaEventHub_Delta/"

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC --select count(*) from VehicleDetails_KafkaEnabledEventHub_Delta
# MAGIC select * from VehicleDetails_KafkaEnabledEventHub_Delta limit 10

# COMMAND ----------

#Creating folder for parquet file in default dbfs location
dbutils.fs.mkdirs("dbfs:/VehiclechData_KafkaEnabledEventHub/parquetFiles/")

# COMMAND ----------

dbutils.fs.ls("dbfs:/VehiclechData_KafkaEnabledEventHub")

# COMMAND ----------

#To get data into parquet location, you need to stop the writestream to delta table and following which you will see inout streaming data will be saved as parquet files in the folder mentioned in the following writestream code
kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("parquet").queryName("veh_details1").option("checkpointLocation", "dbfs:/VehiclechData_KafkaEnabledEventHub/chkpoint/").start("dbfs:/VehiclechData_KafkaEnabledEventHub/parquetFiles")#.awaitTermination()

# COMMAND ----------

# MAGIC %fs ls dbfs:/VehiclechData_KafkaEnabledEventHub/parquetFiles

# COMMAND ----------


