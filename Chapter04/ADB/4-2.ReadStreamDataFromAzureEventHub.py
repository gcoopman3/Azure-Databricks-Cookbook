# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *

#Eventhub namespace and required keys
EH_NAMESPACE_NAME = "CookbookEventHub"
EH_KEY_NAME = "readwrite"
EH_KEY_VALUE = "xxxxxxxxxxxx-xxxxxxx-xxxxx"
# connection string of Event Hubs Namespace
connectionString = "Endpoint=sb://{0}.servicebus.windows.net/;SharedAccessKeyName={1};SharedAccessKey={2};EntityPath=ingestion".format(EH_NAMESPACE_NAME, EH_KEY_NAME, EH_KEY_VALUE)

ehConfig = {}
ehConfig['eventhubs.connectionString'] = connectionString

ehConfig['eventhubs.connectionString'] = sc._jvm.org.apache.spark.eventhubs.EventHubsUtils.encrypt(connectionString)

EventHubdf = spark.readStream.format("eventhubs").options(**ehConfig).load()


# COMMAND ----------

print( EventHubdf.isStreaming)
print( EventHubdf.printSchema())


# COMMAND ----------

jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType())  


# COMMAND ----------

newEventHubDf=EventHubdf.selectExpr("CAST(body AS STRING)")

# COMMAND ----------

newEventHubDf=newEventHubDf.withColumn('vehiclejson', from_json(col('body'),schema=jsonschema))

# COMMAND ----------

newEventHubjsonDF=newEventHubDf.select("body", "vehiclejson.*")

# COMMAND ----------

newEventHubjsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("delta") \
.outputMode("append") \
.option("checkpointLocation", "dbfs:/Vehiclechkpoint_AzureEventHub_Demo/") \
.option("mergeSchema", "true") \
.start("dbfs:/Vehiclechkpoint_AzureEventHub_Delta")

