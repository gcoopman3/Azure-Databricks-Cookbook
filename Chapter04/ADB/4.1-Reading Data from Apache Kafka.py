# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

# We can use to this to reset the offset from where we want to start reading data from kafak provided data in that offset is available in Kafka Source
offset = '''
  {
  "VehicleDetails":{"0": 1}
  }
'''

print(offset)

# COMMAND ----------

#Reading data from kafka source
kafkaDF = spark.readStream.format("kafka") \
.option("kafka.bootstrap.servers", "10.1.0.13:9092,10.1.0.11:9092") \
.option("subscribe", "VehicleDetails") \
.option("group.id", "Cookbook-demo") \
.option("startingOffsets","latest" ) \
.load()

# COMMAND ----------

#Checking if streaming is on and getting the schema for the kakfa dataframe 
print( kafkaDF.isStreaming)
print( kafkaDF.printSchema())

# COMMAND ----------

# display(kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)"))

# COMMAND ----------

#Converting binary datatype to string for the dataframe columns. Without this you cannot use from_json function as it expects the column datatype as string not binary
newkafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

# COMMAND ----------

#Adding new column vehiclejson which is a struct and has 5 columns id, timestamp,rpm,speed and kms
newkafkaDF=newkafkaDF.withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))

# COMMAND ----------

kafkajsonDF=newkafkaDF.select("key","value", "vehiclejson.*")

# COMMAND ----------

#you can run the below command to view the column values
#display(kafkajsonDF)

# COMMAND ----------

#Writing the streaming data to Delta tables
#Location for Delta table is dbfs:/Vehiclechkpoint_Delta. We are using default mount point which is available in Databricks cluster. You can use your own mount point as well. Recommended is to mount external ADLS Gen-2 file system 
kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("delta") \
.outputMode("append") \
.option("checkpointLocation", "dbfs:/Vehiclechkpoint_Demo/") \
.option("mergeSchema", "true") \
.start("dbfs:/Vehiclechkpoint_Delta") 

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC CREATE TABLE IF NOT EXISTS VehicleDetails_Delta
# MAGIC USING DELTA
# MAGIC LOCATION "dbfs:/Vehiclechkpoint_Delta/"

# COMMAND ----------

# MAGIC %sql
# MAGIC --select * from VehicleDetails order by timestamp desc limit 20 --2021-03-03T01:32:36.100+0000
# MAGIC --select * from VehicleDetails_Delta limit 10
# MAGIC select count(*) from VehicleDetails_Delta--1200

# COMMAND ----------

#Creating folder for parquet file in default dbfs location
dbutils.fs.mkdirs("dbfs:/VehiclechData/parquetFiles/")

# COMMAND ----------

dbutils.fs.ls("dbfs:/VehiclechData")

# COMMAND ----------

kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("parquet").queryName("veh_details").option("checkpointLocation", "dbfs:/Vehiclechkpoint_Demo_Parquet1/").start("dbfs:/VehiclechData/parquetFiles")#.awaitTermination()

# COMMAND ----------

# MAGIC %fs ls dbfs:/VehiclechData/parquetFiles

# COMMAND ----------


