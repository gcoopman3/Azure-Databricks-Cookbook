# Databricks notebook source
# MAGIC %md
# MAGIC # TSG100 - The Big Data Cluster troubleshooter
# MAGIC 
# MAGIC ## Description
# MAGIC 
# MAGIC Follow these steps to troubleshoot Big Data Cluster (BDC) issues that
# MAGIC are not covered by the more specific troubleshooters in this chapter.
# MAGIC 
# MAGIC ## Steps
# MAGIC 
# MAGIC ### Get the versions of `azdata`, the BDC and Kubernetes cluster
# MAGIC 
# MAGIC Often the first question asked is “what version are you using”. There
# MAGIC are versions of several things that are useful to know:
# MAGIC 
# MAGIC -   [SOP007 - Version information (azdata, bdc,
# MAGIC     kubernetes)](../common/sop007-get-key-version-information.ipynb)
# MAGIC 
# MAGIC ### Verify `azdata login` works
# MAGIC 
# MAGIC The most fundemental operation that needs to work is `login`. All the
# MAGIC other troubleshooters in this chapter depend on `azdata login` working.
# MAGIC Run this SOP to verify `azdata login` works, this SOP will also analyze
# MAGIC any error output and suggest follow on TSGs as appropriate to help
# MAGIC resolve any issues.
# MAGIC 
# MAGIC -   [SOP028 - azdata login](../common/sop028-azdata-login.ipynb)
# MAGIC 
# MAGIC ### Verify the cluster health monitor is reporting ‘Healthy’
# MAGIC 
# MAGIC -   [TSG078 - Is cluster
# MAGIC     healthy](../diagnose/tsg078-is-cluster-healthy.ipynb)
# MAGIC 
# MAGIC ### Verify that all the pods for ‘Running’
# MAGIC 
# MAGIC Verify the pods for the `kube-system` namespace and the big data cluster
# MAGIC name space are all in the “Running” state, and all the Kubernetes nodes
# MAGIC are “Ready”
# MAGIC 
# MAGIC -   [TSG006 - Get system pod
# MAGIC     status](../monitor-k8s/tsg006-view-system-pod-status.ipynb)
# MAGIC -   [TSG007 - Get BDC pod
# MAGIC     status](../monitor-k8s/tsg007-view-bdc-pod-status.ipynb)
# MAGIC -   [TSG009 - Get nodes
# MAGIC     (Kubernetes)](../monitor-k8s/tsg009-get-nodes.ipynb)
# MAGIC 
# MAGIC ### Verify there are no crash dumps in the cluser
# MAGIC 
# MAGIC The Big Data Cluster should run without any process crashing. Run this
# MAGIC TSG to analyze the entire cluster to verify that no crash dumps have
# MAGIC been created.
# MAGIC 
# MAGIC -   [TSG029 - Find dumps in the
# MAGIC     cluster](../diagnose/tsg029-find-dumps-in-the-cluster.ipynb)
# MAGIC 
# MAGIC ### Next steps
# MAGIC 
# MAGIC This troubleshooter has helped verify the cluster itself is responding
# MAGIC to logins. Use the troubleshooters linked below to drill down into
# MAGIC specific funtionality in the cluster that may not be working correctly.

# COMMAND ----------

# MAGIC %md
# MAGIC Related
# MAGIC -------
# MAGIC 
# MAGIC - [TSG101 - SQL Server troubleshooter](../troubleshooters/tsg101-troubleshoot-sql-server.ipynb)
# MAGIC - [TSG102 - HDFS troubleshooter](../troubleshooters/tsg102-troubleshoot-hdfs.ipynb)
# MAGIC - [TSG103 - Spark troubleshooter](../troubleshooters/tsg103-troubleshoot-spark.ipynb)
# MAGIC - [TSG104 - Control troubleshooter](../troubleshooters/tsg104-troubleshoot-control.ipynb)
# MAGIC - [TSG105 - Gateway troubleshooter](../troubleshooters/tsg105-troubleshoot-gateway.ipynb)
# MAGIC - [TSG106 - App troubleshooter](../troubleshooters/tsg106-troubleshoot-app.ipynb)
