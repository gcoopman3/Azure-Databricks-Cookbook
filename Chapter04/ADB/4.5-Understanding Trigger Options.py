# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

TOPIC = "kafkaenabledhub"
BOOTSTRAP_SERVERS = "kafkaenabledeventhubns.servicebus.windows.net:9093"
EH_SASL = "kafkashaded.org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://kafkaenabledeventhubns.servicebus.windows.net/;SharedAccessKeyName=sendreceivekafka;SharedAccessKey=zzzzzzzz\";"
GROUP_ID = "$Default"

# // Read stream using Spark SQL (structured streaming)
# // consider adding .option("startingOffsets", "earliest") to read earliest available offset during testing
kafkaDF = spark.readStream \
    .format("kafka") \
    .option("subscribe", TOPIC) \
    .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
    .option("kafka.sasl.mechanism", "PLAIN") \
    .option("kafka.security.protocol", "SASL_SSL") \
    .option("kafka.sasl.jaas.config", EH_SASL) \
    .option("kafka.request.timeout.ms", "60000") \
    .option("kafka.session.timeout.ms", "60000") \
    .option("kafka.group.id", "POC") \
    .option("failOnDataLoss", "false") \
    .option("startingOffsets", "latest") \
    .load()




# COMMAND ----------

#Converting binary datatype to string for the dataframe columns. Without this you cannot use from_json function as it expects the column datatype as string not binary
kafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
#Adding new column vehiclejson which is a struct and has 5 columns id, timestamp,rpm,speed and kms
newkafkaDF=kafkaDF.withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))

# COMMAND ----------

kafkajsonDF=newkafkaDF.select("key","value", "vehiclejson.*")

# COMMAND ----------

#you can uncomment and run the below command to view the column values
# display(kafkajsonDF)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Default (No Trigger option specified)

# COMMAND ----------

#Writing the streaming data to Delta tables with default trigger i.e. no trigger option is specified

kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("delta") \
.outputMode("append") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Demo_trigger/") \
.option("mergeSchema", "true") \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta_trigger") 

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC CREATE TABLE IF NOT EXISTS VehicleDetails_KafkaEnabledEventHub_Delta_Trigger
# MAGIC USING DELTA
# MAGIC LOCATION "dbfs:/VehiclechkpointKafkaEventHub_Delta_trigger/"

# COMMAND ----------

# MAGIC %sql
# MAGIC -- You can keep running this cell continuously  and you will find the numbers growing as data is processed as it arrives.
# MAGIC select count(*) from 
# MAGIC VehicleDetails_KafkaEnabledEventHub_Delta_Trigger

# COMMAND ----------

# MAGIC %md
# MAGIC ### Fixed Interval (trigger(processingTime = duration))

# COMMAND ----------

#Cancel the existing write stream query and run the below code
kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("delta") \
.outputMode("append") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Demo_trigger/") \
.option("mergeSchema", "true") \
.trigger(processingTime='30 seconds') \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta_trigger") 


# COMMAND ----------

# MAGIC %sql
# MAGIC -- You can keep running this cell continuously and you will find the numbers don't change continuously , it gets updates after every 30 seconds.
# MAGIC select count(*) from 
# MAGIC VehicleDetails_KafkaEnabledEventHub_Delta_Trigger

# COMMAND ----------

# MAGIC %md
# MAGIC ### continuous (trigger(continuous='1 second'))
# MAGIC - Supported Sources
# MAGIC   - Kafka source: All options are supported.
# MAGIC   - Rate source: Good for testing. Only options that are supported in the continuous mode are numPartitions and rowsPerSecond.
# MAGIC 
# MAGIC - Supported Sinks
# MAGIC   - Kafka sink: All options are supported.
# MAGIC   - Memory sink: Good for debugging.
# MAGIC   - Console sink: Good for debugging. All options are supported. Note that the console will print every checkpoint interval that you have specified in the continuous trigger.

# COMMAND ----------

#Cancel the existing write stream and run the below code. We are writing to console but you can change the snink to Kafka or Memory
kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("console") \
.outputMode("append") \
.trigger(continuous='1 second') \
.start()


# COMMAND ----------

# MAGIC %md
# MAGIC ### Trigger Once (trigger(once=true))

# COMMAND ----------

#Cancel the existing write stream and run the below code. 
#This code will read the stream once since last checkpoint and will stop executing.
kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms" ) \
.writeStream.format("delta") \
.outputMode("append") \
.option("checkpointLocation", "dbfs:/Vehiclechkpointkafkaeventhub_Demo_trigger/") \
.option("mergeSchema", "true") \
.trigger(once=True) \
.start("dbfs:/VehiclechkpointKafkaEventHub_Delta_trigger") 


# COMMAND ----------

# MAGIC %sql
# MAGIC -- You can keep running this cell continuously and you will find the numbers don't change continuously , it gets updates after you rerun the write-stream query.
# MAGIC select count(*) from 
# MAGIC VehicleDetails_KafkaEnabledEventHub_Delta_Trigger
