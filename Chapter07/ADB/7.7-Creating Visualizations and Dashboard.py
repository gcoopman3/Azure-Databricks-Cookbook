# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F
import random
import time

# COMMAND ----------

df_silver_agg=(spark.readStream.format("delta").table("VehicleSensor.VehicleDelta_Silver"))
df_silver_agg.createOrReplaceTempView("vw_AggDetails")

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT Make,Model,coalesce(Borough,"UnKnown")AS Borough,lfi AS LowFuelIndicator,Location,Year,Month,Day,COUNT(*) as TotalVehicles
# MAGIC FROM vw_AggDetails
# MAGIC WHERE lfi=1
# MAGIC GROUP BY Year,Make,Model,coalesce(Borough,"UnKnown"),Location,Month,Day,lfi
# MAGIC ORDER BY Year DESC,Month DESC,TotalVehicles DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT Id as VehicleId,Make,Model,coalesce(Borough,"Unknown") AS Borough,Location,Year,Month,Day,COUNT(*) as TotalVehicles
# MAGIC FROM vw_AggDetails
# MAGIC GROUP BY Make,Model,coalesce(Borough,"Unknown"),Location,Year,Month,Day,Id

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT Make,Borough,Year,Month,COUNT(*) as TotalVehicles
# MAGIC FROM vw_AggDetails
# MAGIC GROUP BY Make,Borough,Year,Month

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT Make,Model,Borough,Location,Year,Month,Day,COUNT(*) as TotalVehicles
# MAGIC FROM vw_AggDetails
# MAGIC GROUP BY Make,Model,Borough,Location,Year,Month,Day

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VehicleSensor.VehicleDeltaAggregated
# MAGIC ORDER BY Month DESC, Day Desc,count desc  limit 10
