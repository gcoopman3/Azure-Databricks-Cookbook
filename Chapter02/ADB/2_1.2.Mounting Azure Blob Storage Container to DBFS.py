# Databricks notebook source
#Storage account and key you will get it from the portal as shown in the Cookbook Recipe.
storageAccount="cookbookblobstorage"
storageKey ="xx-xx-xxx"
mountpoint = "/mnt/Blob"
storageEndpoint =   "wasbs://rawdata@{}.blob.core.windows.net".format(storageAccount)
storageConnSting = "fs.azure.account.key.{}.blob.core.windows.net".format(storageAccount)

try:
  dbutils.fs.mount(
  source = storageEndpoint,
  mount_point = mountpoint,
  extra_configs = {storageConnSting:storageKey})
except:
    print("Already mounted...."+mountpoint)


# COMMAND ----------

# MAGIC %fs ls /mnt/Blob

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Blob"))

# COMMAND ----------

#Lets read data from csv file which is copied to Blob Storage
df_ord= spark.read.format("csv").option("header",True).load("dbfs:/mnt/Blob/Orders.csv")

# COMMAND ----------

display(df_ord.limit(10))

# COMMAND ----------

dbutils.fs.unmount("/mnt/Blob")
