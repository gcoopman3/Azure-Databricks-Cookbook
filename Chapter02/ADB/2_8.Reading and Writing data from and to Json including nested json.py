# Databricks notebook source
storageAccount="cookbookstoragegen2"
mountpoint = "/mnt/SensorData/JsonData"
storageEndPoint ="abfss://sensordata@{}.dfs.core.windows.net/".format(storageAccount)
print ('Mount Point ='+mountpoint)

#ClientId, TenantId and Secret is for the Application(ADLSGen2App) was have created as part of this recipe
clientID ="xxxxxx"
tenantID ="xxxxx"
clientSecret ="xxxxx"
oauth2Endpoint = "https://login.microsoftonline.com/{}/oauth2/token".format(tenantID)


configs = {"fs.azure.account.auth.type": "OAuth",
           "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
           "fs.azure.account.oauth2.client.id": clientID,
           "fs.azure.account.oauth2.client.secret": clientSecret,
           "fs.azure.account.oauth2.client.endpoint": oauth2Endpoint}

try:
  dbutils.fs.mount(
  source = storageEndPoint,
  mount_point = mountpoint,
  extra_configs = configs)
except:
    print("Already mounted...."+mountpoint)


# COMMAND ----------

dbutils.fs.unmount("/mnt/SensorData/JsonData")

# COMMAND ----------

display(dbutils.fs.ls("dbfs:/mnt/SensorData/JsonData"))

# COMMAND ----------

df_json = spark.read.option("multiline","true").json("/mnt/SensorData/JsonData/SimpleJsonData/")
display(df_json)

# COMMAND ----------

df_json.write.format("json").mode("overwrite").save("/mnt/SensorData/JsonData/SimpleJsonData/")

# COMMAND ----------

df_json = spark.read.option("multiline","true").json("dbfs:/mnt/SensorData/JsonData/JsonData/")
display(df_json)

# COMMAND ----------

from pyspark.sql.functions import explode
data_df = df_json.select("_id",   
    explode("owners").alias("vehicleOwnersExplode")
).select("_id","vehicleOwnersExplode.*")
display(data_df)

# COMMAND ----------

jsonDF = data_df.withColumn("jsonCol",
                            to_json(struct([data_df[x] for x in data_df.columns])))
                            .select("jsonCol")
display(jsonDF)

# COMMAND ----------

from pyspark.sql.functions import explode
#dft=df_json.select(df_json._id,explode(df_json.owners)).show(truncate=False)


data_df = df_json.select(
    "Fuel",
    "Transmission",
    "_id",
    "about",
    "address",
    "color",
    "cost",
    "currentowner",
    "eventtime",
    "index",
    "latitude",
    "longitude",
    "phone",
    "seatingcapacity",
    "sellingcompany",
    "vehicleid",
    "vehiclename",
    explode("owners").alias("sensorReadingsExplode")
).select("_id", "Fuel",
    "Transmission",
    "_id",
    "about",
    "address",
    "color",
    "cost",
    "currentowner",
    "eventtime",
    "index",
    "latitude",
    "longitude",
    "phone",
    "seatingcapacity",
    "sellingcompany",
    "vehicleid",
    "vehiclename", "sensorReadingsExplode.*")
#df_json.select(explode(df_json.owners)).show(truncate=False)
