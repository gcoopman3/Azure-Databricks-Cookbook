# Databricks notebook source
from pyspark.sql.types import *

# COMMAND ----------

#You will find the Storage account and storage key from the portal as shown in the Cookbook Recipe.
storageAccount="cookbookblobstorage"
storageKey ="xx-xx-xx-xx"
mountpoint = "/mnt/Blob"
storageEndpoint =   "wasbs://rawdata@{}.blob.core.windows.net".format(storageAccount)
storageConnSting = "fs.azure.account.key.{}.blob.core.windows.net".format(storageAccount)
parquetCustomerDestMount = "{}/Customer/parquetFiles".format(mountpoint)

try:
  dbutils.fs.mount(
  source = storageEndpoint,
  mount_point = mountpoint,
  extra_configs = {storageConnSting:storageKey})
except:
    print("Already mounted...."+mountpoint)


# COMMAND ----------

# MAGIC %fs ls /mnt/Blob

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Blob/Customer/csvFiles/"))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading CSV files from mount point

# COMMAND ----------

#Lets read data from csv file which is copied to Blob Storage. Here we are not specifying any schema and default it will be all string types
df_cust= spark.read.format("csv").option("header",True).load("/mnt/Blob/Customer/csvFiles/")

# COMMAND ----------

# All datatypes are of type string
df_cust.printSchema()

# COMMAND ----------

#Lets read data from csv file which is copied to Blob Storage. Here we are not specifying any schema and asking spark to infer the schema
df_cust= spark.read.format("csv").option("header",True).option("inferSchema", True).load("/mnt/Blob/Customer/csvFiles/")

# COMMAND ----------

#checking the datatypes 
df_cust.printSchema()

# COMMAND ----------

#printing 10 records from the dataframe
display(df_cust.limit(10))

# COMMAND ----------

#Creating a schema which can be passed while creating the dataframe
cust_schema = StructType([
    StructField("C_CUSTKEY", IntegerType()),
    StructField("C_NAME", StringType()),
    StructField("C_ADDRESS", StringType()),
    StructField("C_NATIONKEY", ShortType()),
    StructField("C_PHONE", StringType()),
    StructField("C_ACCTBAL", DoubleType()),
    StructField("C_MKTSEGMENT", StringType()),
    StructField("C_COMMENT", StringType())
])

# COMMAND ----------

#Lets read data from csv files by specifying the schema explicitly
df_cust= spark.read.format("csv").option("header",True).schema(cust_schema).load("/mnt/Blob/Customer/csvFiles/")

# COMMAND ----------

#You can see thath we have used short datatype for NationKey rather than interger type which spark is inferring by default
df_cust.printSchema()

# COMMAND ----------

display(df_cust.limit(10))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Writing data to Azure Blob the mount point to DBFS

# COMMAND ----------

# We are repartitioning to ensure we are creating 10 files in parquet folder
# We are writing the dataframe to Azure Blob as parquet format. 
df_cust_partitioned=df_cust.repartition(10)
df_cust_partitioned.write.mode("overwrite").option("header", "true").parquet(parquetCustomerDestMount)

# COMMAND ----------

#Lets view the parquet files that are created in the mount point
display(dbutils.fs.ls(parquetCustomerDestMount))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading csvfiles directly from Azure Blob Storage using Dataframe API without using the mount point

# COMMAND ----------

#Setting up account access key so that we can directly access the blob storage 
spark.conf.set(
 storageConnSting,
  storageKey)

# COMMAND ----------

#Reading the files and folders in Cutomer folder
storageEndpointFolders = "{}/Customer".format(storageEndpoint)
display(dbutils.fs.ls(storageEndpointFolders))

# COMMAND ----------

#Reading the files directly from the Azure Blob storage using wasbs path
df_cust= spark.read.format("csv").option("header",True).schema(cust_schema).load("wasbs://rawdata@cookbookblobstorage.blob.core.windows.net/Customer/csvFiles/")

# COMMAND ----------

display(df_cust.limit(10))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Writing data to directly to Azure Blob without using the mount point

# COMMAND ----------

#Lets write the dataframe output to Azure blob directly as parquet format without using the mount point
parquetCustomerDestDirect = "wasbs://rawdata@cookbookblobstorage.blob.core.windows.net/Customer/csvFiles/parquetFilesDirect"
df_cust_partitioned_direct=df_cust.repartition(10)
df_cust_partitioned_direct.write.mode("overwrite").option("header", "true").parquet(parquetCustomerDestDirect)


# COMMAND ----------

#View the parquet files which are created directly on Azure blob
display(dbutils.fs.ls(parquetCustomerDestDirect))

# COMMAND ----------

# Direct access using SAS 
#Setting up shared access signature so that we can directly access the blob storage 
# rawdata is the name of the conatiner in the Azure Blob storage that we have created
storageConnSting = "fs.azure.sas.rawdata.{}.blob.core.windows.net".format(storageAccount)
spark.conf.set(
 storageConnSting,
  "?sv=2019-12-12&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-02-01T02:33:49Z&st=2021-01-31T18:33:49Z&spr=https&sig=xxxxxxxxxxxxx")

# COMMAND ----------

#Reading the files and folders in Cutomer folder
storageEndpointFolders = "{}/Customer".format(storageEndpoint)
display(dbutils.fs.ls(storageEndpointFolders))

# COMMAND ----------


