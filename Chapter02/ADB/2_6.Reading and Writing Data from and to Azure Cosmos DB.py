# Databricks notebook source
display(dbutils.fs.ls("/mnt/Gen2Source/Customer/csvFiles")) 

# COMMAND ----------

#Reading csv files into a DataFrame
customerDF = spark.read.format("csv").option("header",True).option("inferSchema", True).load("dbfs:/mnt/Gen2Source/Customer/csvFiles")

# COMMAND ----------

# MAGIC %md
# MAGIC #### Writing CSV Files to Cosmos DB

# COMMAND ----------

writeConfig = {
 "Endpoint" : "https://cookbookdemocosmosdb.documents.azure.com:443/",
 "Masterkey" : "xxx-xx-xxxx",
 "Database" : "Sales", 
 "Collection" : "Customer",
 "Upsert" : "true"
}


# COMMAND ----------

#Writing DataFrame to Cosmos DB. If the Comos DB RU's are less then it will take quite some time to write 150K records. We are using save mode as append.
customerDF.write.format("com.microsoft.azure.cosmosdb.spark") \
.options(**writeConfig)\
.mode("append")\
.save()

# COMMAND ----------

#Writing DataFrame to Cosmos DB. If the Comos DB RU's are less then it will take quite some time to write 150K records. We are using save mode as overwrite.
customerDF.write.format("com.microsoft.azure.cosmosdb.spark") \
.options(**writeConfig)\
.mode("overwrite")\
.save()

# COMMAND ----------

# MAGIC %md
# MAGIC #### Reading from Cosmos DB

# COMMAND ----------

df_Customer.count()

# COMMAND ----------

readConfig = {
 "Endpoint" : "https://cookbookdemocosmosdb.documents.azure.com:443/",
 "Masterkey" : "xx-xxx-xxx-xxx",
  "Database" : "Sales", 
  "Collection" : "Customer",
  "preferredRegions" : "Central US;East US2",
  "SamplingRatio" : "1.0",
  "schema_samplesize" : "1000",
  "query_pagesize" : "2147483647",
  "query_custom" : "SELECT * FROM c where c.C_MKTSEGMENT ='AUTOMOBILE'" # 
}

# Connect via azure-cosmosdb-spark to create Spark DataFrame
df_Customer = spark.read.format("com.microsoft.azure.cosmosdb.spark").options(**readConfig).load()
df_Customer.count()

# COMMAND ----------

display(df_Customer.limit(5))

# COMMAND ----------


