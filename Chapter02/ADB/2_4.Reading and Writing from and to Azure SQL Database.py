# Databricks notebook source
# MAGIC %md
# MAGIC ### Create the below table in Azure SQL DB before proceeding with the below steps
# MAGIC 
# MAGIC <p>   CREATE TABLE CUSTOMER 
# MAGIC         (
# MAGIC <br>    C_CUSTKEY		INT,
# MAGIC <br>	C_NAME			VARCHAR(25),
# MAGIC <br>	C_ADDRESS		VARCHAR(40),
# MAGIC <br>	C_NATIONKEY		SMALLINT,
# MAGIC <br>	C_PHONE			CHAR(15),
# MAGIC <br>	C_ACCTBAL		DECIMAL,
# MAGIC <br>	C_MKTSEGMENT	CHAR(10),
# MAGIC <br>	C_COMMENT		VARCHAR(117)
# MAGIC         );</p>

# COMMAND ----------

from pyspark.sql.types import *

# COMMAND ----------

# Details about connection string
logicalServername = "devtestlogicalserver.database.windows.net"
databaseName = "demoDB"
tableName = "dbo.CUSTOMER"
userName = "sqluser"
password = "StrongPwd" # Please specify password here
jdbcUrl = "jdbc:sqlserver://{0}:{1};database={2}".format(logicalServername, 1433, databaseName)
connectionProperties = {
  "user" : userName,
  "password" : password,
  "driver" : "com.microsoft.sqlserver.jdbc.SQLServerDriver"
}


# COMMAND ----------

#Creating a schema which can be passed while creating the dataframe
cust_schema = StructType([
    StructField("C_CUSTKEY", IntegerType()),
    StructField("C_NAME", StringType()),
    StructField("C_ADDRESS", StringType()),
    StructField("C_NATIONKEY", ShortType()),
    StructField("C_PHONE", StringType()),
    StructField("C_ACCTBAL", DecimalType(18,2)),
    StructField("C_MKTSEGMENT", StringType()),
    StructField("C_COMMENT", StringType())
])

# COMMAND ----------

# Reading customer csv files in a dataframe. This Dataframe will be written to Customer table in Azure SQL DB
df_cust= spark.read.format("csv").option("header",True).schema(cust_schema).load("dbfs:/mnt/Gen2Source/Customer/csvFiles")

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading and writing data from and to Customers table using JDBC SQLServer Drivers

# COMMAND ----------

#Reading the table we have created and we shouldn;t see any rows in the dataframe
df_jdbcRead= spark.read.jdbc(jdbcUrl,  
                   table=tableName, 
                   properties=connectionProperties)

display(df_jdbcRead.printSchema())

# COMMAND ----------

#It shouldn't fetch any records as there is no data loaded into the table
display(df_jdbcRead.limit(5))

# COMMAND ----------

# Writing the dataframe which has all the csv read from ADLS Gen-2 folder to Customer table in Azure SQL Db
df_cust.write.jdbc(jdbcUrl,  
                   mode ="append", 
                   table=tableName, 
                   properties=connectionProperties)

# COMMAND ----------

#Lets read the table after loading the data to count the number of rows
df_jdbcRead= spark.read.jdbc(jdbcUrl,  
                   table=tableName, 
                   properties=connectionProperties)

#Counting number of rows
df_jdbcRead.count()

# COMMAND ----------

display(df_jdbcRead.limit(5))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading and writing data from and to Customers table using Apache Spark connector for Azure SQL

# COMMAND ----------

server_name = f"jdbc:sqlserver://{logicalServername}" 
database_name = "demoDB"
url = server_name + ";" + "databaseName=" + database_name + ";"

table_name = "dbo.Customer"
username = "sqladmin"
password = "Password@12345" # Please specify password here

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading data from Customers table using Apache Spark connector

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Truncate the dbo.Customer table so that we are loading data into empty table.

# COMMAND ----------

# Truncate the dbo.Customer table so that we are loading data into empty table.
sparkconnectorDF = spark.read \
        .format("com.microsoft.sqlserver.jdbc.spark") \
        .option("url", url) \
        .option("dbtable", table_name) \
        .option("user", username) \
        .option("password", password).load()

# COMMAND ----------

display(sparkconnectorDF.printSchema())

# COMMAND ----------

display(sparkconnectorDF.limit(5))

# COMMAND ----------

#This code is truncating the table and loading the data. If we need to preserve the indexes created in this table then when should use the .option("truncate",True) which will avoid dropping the index
try:
  df_cust.write \
    .format("com.microsoft.sqlserver.jdbc.spark") \
    .mode("overwrite") \
    .option("truncate",True) \
    .option("url", url) \
    .option("dbtable", tableName) \
    .option("user", userName) \
    .option("password", password) \
    .save()
except ValueError as error :
    print("Connector write failed", error)

# COMMAND ----------

display(sparkconnectorDF.limit(5))

# COMMAND ----------

#Appending records to the existing table
try:
  df_cust.write \
    .format("com.microsoft.sqlserver.jdbc.spark") \
    .mode("append") \
    .option("url", url) \
    .option("dbtable", tableName) \
    .option("user", userName) \
    .option("password", password) \
    .save()
except ValueError as error :
    print("Connector write failed", error)

# COMMAND ----------

#Counting number of rows, it should be 300K.
df_jdbcRead.count()

# COMMAND ----------

#Lets read the data from the table
sparkconnectorDF = spark.read \
        .format("com.microsoft.sqlserver.jdbc.spark") \
        .option("url", url) \
        .option("dbtable", table_name) \
        .option("user", username) \
        .option("password", password).load()

# COMMAND ----------

display(sparkconnectorDF.limit(10))

# COMMAND ----------


