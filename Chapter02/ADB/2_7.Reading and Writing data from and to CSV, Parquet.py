# Databricks notebook source
# MAGIC %md
# MAGIC ### Reading data from CSV Files and Writing to CSV Files

# COMMAND ----------

#Listing CSV Files
dbutils.fs.ls("/mnt/Gen2Source/Customer/csvFiles")

# COMMAND ----------

customerDF = spark.read.format("csv").option("header",True).option("inferSchema", True).load("/mnt/Gen2Source/Customer/csvFiles")

# COMMAND ----------

customerDF.show()

# COMMAND ----------

customerDF.write.mode("overwrite").option("header", "true").csv("/mnt/Gen2Source/Customer/WriteCsvFiles")

# COMMAND ----------

#Reading from the preceding location to ensure data is saved properly in target folder.
targetDF = spark.read.format("csv").option("header",True).option("inferSchema", True).load("/mnt/Gen2Source/Customer/WriteCsvFiles")

# COMMAND ----------

targetDF.show()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading data from Parquet Files and Writing to Parquet Files

# COMMAND ----------

targetDF.count()

# COMMAND ----------

#Writing the targetDF data which has the CSV data read as parquet File using append mode

targetDF.write.mode("overwrite").option("header", "true").parquet("/mnt/Gen2Source/Customer/csvasParquetFiles/")


# COMMAND ----------

df_parquetfiles=spark.read.format("parquet").option("header",True).load("/mnt/Gen2Source/Customer/csvasParquetFiles/")
# 

# COMMAND ----------

display(df_parquetfiles.limit(5))

# COMMAND ----------

#Using overwrite as option for save mode
targetDF.write.mode("append").option("header", "true").parquet("/mnt/Gen2Source/Customer/csvasParquetFiles/")

# COMMAND ----------

df_parquetfiles=spark.read.format("parquet").option("header",True).load("/mnt/Gen2Source/Customer/csvasParquetFiles/")
# 

# COMMAND ----------

# Append has loaded the same data and the count has doubled
df_parquetfiles.count()

# COMMAND ----------


