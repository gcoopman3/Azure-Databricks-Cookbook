# Databricks notebook source
storageAccount="cookbookstoragegen2"
mountpoint = "/mnt/Gen2Source"
storageEndPoint ="abfss://rawdata@{}.dfs.core.windows.net/".format(storageAccount)
print ('Mount Point ='+mountpoint)

#ClientId, TenantId and Secret is for the Application(ADLSGen2App) was have created as part of this recipe
clientID ="xxx-xx-xxx"
tenantID ="xxx-xxx-xxxx"
clientSecret ="xx-xx-xxx-xxx"
oauth2Endpoint = "https://login.microsoftonline.com/{}/oauth2/token".format(tenantID)

configs = {"fs.azure.account.auth.type": "OAuth",
"fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
"fs.azure.account.oauth2.client.id": clientID,
"fs.azure.account.oauth2.client.secret": clientSecret,
"fs.azure.account.oauth2.client.endpoint": oauth2Endpoint}

dbutils.fs.mount(
source = storageEndPoint,
mount_point = mountpoint,
extra_configs = configs)


# COMMAND ----------

# dbutils.fs.unmount("/mnt/Gen2Source")

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Gen2Source/Customer/csvFiles"))

# COMMAND ----------

blobStorage = "cookbookblobstorage1.blob.core.windows.net"
blobContainer = "synapse"
blobAccessKey = "xx-xxx-xxx"


# COMMAND ----------

tempDir = "wasbs://" + blobContainer + "@" + blobStorage +"/tempDirs"

# COMMAND ----------

acntInfo = "fs.azure.account.key."+ blobStorage
#Setting Blob storage acces key for this notebook
spark.conf.set(
  acntInfo,
  blobAccessKey)


# COMMAND ----------

customerDF = spark.read.format("csv").option("header",True).option("inferSchema", True).load("dbfs:/mnt/Gen2Source/Customer/csvFiles")

# COMMAND ----------

# We have changed trustServerCertificate=true from trustServerCertificate=false. In certain cases you might get error 
'''Py4JJavaError: An error occurred while calling o390.save.
: com.databricks.spark.sqldw.SqlDWSideException: Azure Synapse Analytics failed to execute the JDBC query produced by the connector.
Underlying SQLException(s):
  - com.microsoft.sqlserver.jdbc.SQLServerException: The driver could not establish a secure connection to SQL Server by using Secure Sockets Layer (SSL) encryption. Error: "Failed to validate the server name in a certificate during Secure Sockets Layer (SSL) initialization.". ClientConnectionId:3a34e73d-7b06-44c6-b23c-a581567f93f5 [ErrorCode = 0] [SQLState = 08S '''
  
sqlDwUrl="jdbc:sqlserver://synapsedemoworkspace11.sql.azuresynapse.net:1433;database=sqldwpool1;user=sqladminuser@synapsedemoworkspacetest;password=TestStrongPwd;encrypt=true;trustServerCertificate=true;hostNameInCertificate=*.sql.azuresynapse.net;loginTimeout=30;"

db_table = "dbo.customer"

# COMMAND ----------

# This code is writing to data into SQL Pool with default save option. In the default save option it check if the table name exists then it errors out else it will create a table and populates the data.
customerDF.write \
  .format("com.databricks.spark.sqldw")\
  .option("url", sqlDwUrl)\
  .option("forwardSparkAzureStorageCredentials", "true")\
  .option("dbTable", db_table)\
  .option("tempDir", tempDir)\
  .save()


# COMMAND ----------

# This code is writing to data into SQL Pool with append save option. In append save option data is appended to existing table.
customerDF.write \
  .format("com.databricks.spark.sqldw")\
  .option("url", sqlDwUrl)\
  .option("forwardSparkAzureStorageCredentials", "true")\
  .option("dbTable", db_table)\
  .option("tempDir", tempDir)\
  .mode("append")\
  .save()

# COMMAND ----------

# This code is writing to data into SQL Pool with over save option. In the default save option it check if the table name exists then it errors out else it will create a table and populates the data.
customerDF.write \
  .format("com.databricks.spark.sqldw")\
  .option("url", sqlDwUrl)\
  .option("forwardSparkAzureStorageCredentials", "true")\
  .option("dbTable", db_table)\
  .option("tempDir", tempDir)\
  .mode("overwrite")\
  .save()

# COMMAND ----------

# customerTabledf = spark.read
#   .format("com.databricks.spark.sqldw")
#   .option("url", "jdbc:sqlserver://azureaynapseanalyticssvr.database.windows.net:1433;database=AzureSynapseAnalyticsDW;user=xxx@azureaynapseanalyticssvr;password=xxxx;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;")
#   .option("tempDir", "wasbs://rawdata@stacccookbook.blob.core.windows.net/tempDirs")
#   .option("forwardSparkAzureStorageCredentials", "true")
#   .option("dbTable", "CustomerTable")
#   .load()
  
  # Get some data from an Azure Synapse table.
customerTabledf = spark.read \
  .format("com.databricks.spark.sqldw") \
  .option("url", sqlDwUrl) \
  .option("tempDir", tempDir) \
  .option("forwardSparkAzureStorageCredentials", "true") \
  .option("dbTable", db_table) \
  .load()

# COMMAND ----------

customerTabledf.show()

# COMMAND ----------

query= " select C_MKTSEGMENT, count(*) as Cnt from [dbo].[customer] group by C_MKTSEGMENT"
df_query = spark.read \
  .format("com.databricks.spark.sqldw") \
  .option("url", sqlDwUrl) \
  .option("tempDir", tempDir) \
  .option("forwardSparkAzureStorageCredentials", "true") \
  .option("query", query) \
  .load()

# COMMAND ----------

display(df_query.limit(5))
