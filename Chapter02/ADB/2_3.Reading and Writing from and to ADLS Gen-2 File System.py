# Databricks notebook source
from pyspark.sql.types import *
from pyspark.sql.functions import col
from pyspark.sql.functions import col,sum,avg,max

# COMMAND ----------

storageAccount="cookbookadlsgen2storage"
mountpoint = "/mnt/Gen2"
storageEndPoint ="abfss://rawdata@{}.dfs.core.windows.net/".format(storageAccount)
print ('Mount Point ='+mountpoint)

#ClientId, TenantId and Secret is for the Application(ADLSGen2App) was have created as part of this recipe
clientID ="xxx-xx-xxx-xxx" #Called as Application Id as well
tenantID ="xx-xx-xx-xxx"
clientSecret ="xxx-xx-xxxxx"
oauth2Endpoint = "https://login.microsoftonline.com/{}/oauth2/token".format(tenantID)


configs = {"fs.azure.account.auth.type": "OAuth",
           "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
           "fs.azure.account.oauth2.client.id": clientID,
           "fs.azure.account.oauth2.client.secret": clientSecret,
           "fs.azure.account.oauth2.client.endpoint": oauth2Endpoint}

try:
  dbutils.fs.mount(
  source = storageEndPoint,
  mount_point = mountpoint,
  extra_configs = configs)
except:
    print("Already mounted...."+mountpoint)



# COMMAND ----------

# MAGIC %fs ls /mnt/Gen2

# COMMAND ----------

display(dbutils.fs.ls("dbfs:/mnt/Gen2/Customer/csvFiles"))

# COMMAND ----------

# Reading customer csf files in a dataframe
df_cust= spark.read.format("csv").option("header",True).load("dbfs:/mnt/Gen2/Customer/csvFiles")


# COMMAND ----------

display(df_cust.limit(10))

# COMMAND ----------

#Creating the required schema that will be use while reading the csv files
cust_schema = StructType([
    StructField("C_CUSTKEY", IntegerType()),
    StructField("C_NAME", StringType()),
    StructField("C_ADDRESS", StringType()),
    StructField("C_NATIONKEY", ShortType()),
    StructField("C_PHONE", StringType()),
    StructField("C_ACCTBAL", DoubleType()),
    StructField("C_MKTSEGMENT", StringType()),
    StructField("C_COMMENT", StringType())
])


# COMMAND ----------

df_cust= spark.read.format("csv").option("header",True).schema(cust_schema).load("/mnt/Gen2/Customer/csvFiles")

# COMMAND ----------

display(df_cust.limit(10))

# COMMAND ----------

# Now lets aggregate the account balance based on the market segment and save the results as parquet format
df_cust_agg = df_cust.groupBy("C_MKTSEGMENT") \
    .agg(sum("C_ACCTBAL").cast('decimal(20,3)').alias("sum_acctbal"), \
      avg("C_ACCTBAL").alias("avg_acctbal"), \
      max("C_ACCTBAL").alias("max_bonus")).orderBy("avg_acctbal",ascending=False)

# COMMAND ----------

df_cust_agg.show()

# COMMAND ----------

df_cust_agg.write.mode("overwrite").option("header", "true").csv("/mnt/Gen-2/CustMarketSegmentAgg/")

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Gen-2/CustMarketSegmentAgg/"))

# COMMAND ----------

df_custagg= spark.read.format("csv").option("header",True).load("dbfs:/mnt/Gen-2/CustAgg1/")

# COMMAND ----------

df_custagg.show()

# COMMAND ----------

# MAGIC %md
# MAGIC ## Accessing files directly from ADLS Gen-2 without mounting to DBFS
# MAGIC #### In the below section we will see how to access ADLS Gen-2 storage accout directly using OAuth2.0 using Service Principal. We can access any ADLS Gen-2 account that the service principal has access to.

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.cookbookadlsgen2storage.dfs.core.windows.net", "OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.cookbookadlsgen2storage.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.cookbookadlsgen2storage.dfs.core.windows.net", clientID)
spark.conf.set("fs.azure.account.oauth2.client.secret.cookbookadlsgen2storage.dfs.core.windows.net", clientSecret)
spark.conf.set("fs.azure.account.oauth2.client.endpoint.cookbookadlsgen2storage.dfs.core.windows.net", oauth2Endpoint)

# COMMAND ----------

#Directly accessing the ADLS Gen-2 fileystem and reading data from specific folder 
df_direct = spark.read.format("csv").option("header",True).schema(cust_schema).load("abfss://rawdata@cookbookadlsgen2storage.dfs.core.windows.net/Customer/csvFiles")

# COMMAND ----------

display(df_direct.limit(10))

# COMMAND ----------

parquetCustomerDestDirect = "abfss://rawdata@cookbookadlsgen2storage.dfs.core.windows.net/Customer/parquetFiles"
df_direct_repart=df_direct.repartition(10)
df_direct_repart.write.mode("overwrite").option("header", "true").parquet(parquetCustomerDestDirect)

# COMMAND ----------

display(dbutils.fs.ls(parquetCustomerDestDirect))

# COMMAND ----------

#Reading the parquet files created in preceding step
df_parquet = spark.read.format("parquet").option("header",True).schema(cust_schema).load("abfss://rawdata@cookbookadlsgen2storage.dfs.core.windows.net/Customer/parquetFiles")

# COMMAND ----------

display(df_parquet.limit(10))

# COMMAND ----------


