# Databricks notebook source
storageAccount="cookbookadlsgen2storage1"
mountpoint = "/mnt/Gen2Source"
storageEndPoint ="abfss://rawdata@{}.dfs.core.windows.net/".format(storageAccount)
print ('Mount Point ='+mountpoint)

#ClientId, TenantId and Secret is for the Application(ADLSGen2App) was have created as part of this recipe
clientID =""
tenantID =""
clientSecret =""
oauth2Endpoint = "https://login.microsoftonline.com/{}/oauth2/token".format(tenantID)


configs = {"fs.azure.account.auth.type": "OAuth",
           "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
           "fs.azure.account.oauth2.client.id": clientID,
           "fs.azure.account.oauth2.client.secret": clientSecret,
           "fs.azure.account.oauth2.client.endpoint": oauth2Endpoint}

try:
  dbutils.fs.mount(
  source = storageEndPoint,
  mount_point = mountpoint,
  extra_configs = configs)
except e:
  print("Already Mounted")
    


# COMMAND ----------

from pyspark.sql.types import *
from pyspark.sql.functions import col
from pyspark.sql.functions import col,sum,avg,max

# COMMAND ----------

df_parquet = spark.read.format("parquet").option("header",True).load("/mnt/Gen2Source/Customer/parquetFiles")

# COMMAND ----------

display(df_parquet.show(10))

# COMMAND ----------

for i in range(1,20):
  df_parquet.write.format("delta").mode("append").save("/mnt/Gen2Source/CustomerDelta")

# COMMAND ----------

#Creating CustomerDelta table
spark.sql("CREATE TABLE CustomerDelta USING DELTA LOCATION '/mnt/Gen2Source/CustomerDelta/'")

# COMMAND ----------

for i in range(1,20):
  df_parquet.write.format("delta").partitionBy("C_MKTSEGMENT").mode("append").save("/mnt/Gen2Source/CustomerDeltaPartition")

# COMMAND ----------

#Creating CustomerDeltaPartition table
spark.sql("CREATE TABLE CustomerDeltaPartition USING DELTA LOCATION '/mnt/Gen2Source/CustomerDeltaPartition/'")

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Updating non partitioned table
# MAGIC -- Immediately go to the Notebook 6_4_1.ConcurrencyTest and run the first cell (DELETE FROM CustomerDelta WHERE C_MKTSEGMENT='FURNITURE')
# MAGIC UPDATE CustomerDelta SET C_NATIONKEY = 4 WHERE C_MKTSEGMENT='MACHINERY'

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Updating non partitioned table
# MAGIC -- Immediately go to the Notebook 6_4_1.ConcurrencyTest and run the second cell (DELETE FROM CustomerDeltaPartition WHERE C_MKTSEGMENT='FURNITURE')
# MAGIC UPDATE CustomerDeltaPartition SET C_NATIONKEY = 4 WHERE C_MKTSEGMENT='MACHINERY'

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC ALTER TABLE CustomerDeltaPartition SET TBLPROPERTIES ('delta.isolationLevel' = 'Serializable')

# COMMAND ----------

# After running this cell, Immediately go to the Notebook 6_4_1.ConcurrencyTest and run the third cell (DELETE FROM CustomerDeltaPartition WHERE C_MKTSEGMENT='MACHINERY' AND C_CUSTKEY=1377)
for i in range(1,20):
  df_parquet.where(col("C_MKTSEGMENT")=="MACHINERY").write.format("delta").mode("append").partitionBy("C_MKTSEGMENT").save("/mnt/Gen2Source/CustomerDeltaPartition")


# COMMAND ----------



# COMMAND ----------

# MAGIC %sql
# MAGIC ALTER TABLE CustomerDeltaPartition SET TBLPROPERTIES ('delta.isolationLevel' = 'WriteSerializable')

# COMMAND ----------


