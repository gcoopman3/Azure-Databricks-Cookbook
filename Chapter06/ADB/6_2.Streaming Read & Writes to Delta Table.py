# Databricks notebook source
from pyspark.sql.functions import *
from pyspark.sql.types import *
from datetime import datetime
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F
import random

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

# MAGIC %fs mkdirs /mnt/Gen2Source/Vehicle_Delta/Chkpnt

# COMMAND ----------

def checkpoint_dir(): 
  return "/mnt/Gen2Source/Vehicle_Delta/Chkpnt/%s" % str(random.randint(0, 10000))
 

# COMMAND ----------

BOOTSTRAP_SERVERS = "kafkaenabledeventhubns.servicebus.windows.net:9093"
EH_SASL = "kafkashaded.org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://kafkaenabledeventhubns.servicebus.windows.net/;SharedAccessKeyName=sendreceivekafka;SharedAccessKey=zzzzzzzz\";"
GROUP_ID = "$Default"


# COMMAND ----------

# Function to read data from EventHub and writing as delta format
def append_kafkadata_stream(topic="eventhubsource1"):
  kafkaDF = (spark.readStream \
    .format("kafka") \
    .option("subscribe", topic) \
    .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
    .option("kafka.sasl.mechanism", "PLAIN") \
    .option("kafka.security.protocol", "SASL_SSL") \
    .option("kafka.sasl.jaas.config", EH_SASL) \
    .option("kafka.request.timeout.ms", "60000") \
    .option("kafka.session.timeout.ms", "60000") \
    .option("kafka.group.id", GROUP_ID) \
    .option("failOnDataLoss", "false") \
    .option("startingOffsets", "latest") \
    .load().withColumn("source", lit(topic)))
  
  newkafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)","source").withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))
  kafkajsonDF=newkafkaDF.select("key","value","source", "vehiclejson.*")
  
  query=kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms"
                  ,"source") \
            .writeStream.format("delta") \
            .outputMode("append") \
            .option("checkpointLocation",checkpoint_dir()) \
            .start("/mnt/Gen2Source/Vehicle_Delta/") 
 
  return query

# COMMAND ----------

query_source1 = append_kafkadata_stream(topic='eventhubsource1')


# COMMAND ----------

query_source2 = append_kafkadata_stream(topic='eventhubsource2')

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC CREATE DATABASE IF NOT EXISTS Vehicle;
# MAGIC CREATE TABLE IF NOT EXISTS Vehicle.VehicleDetails_Delta
# MAGIC USING DELTA
# MAGIC LOCATION "/mnt/Gen2Source/Vehicle_Delta/"

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC --select count(*) from VehicleDetails_KafkaEnabledEventHub_Delta
# MAGIC select count(*),source from Vehicle.VehicleDetails_Delta group by source

# COMMAND ----------

display(spark.readStream.format("delta").table("Vehicle.VehicleDetails_Delta").groupBy("source").count().orderBy("source"))
