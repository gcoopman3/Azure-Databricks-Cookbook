# Databricks notebook source
# MAGIC %sql
# MAGIC DROP TABLE IF EXISTS customer;
# MAGIC CREATE TABLE customer(
# MAGIC   C_CUSTKEY INT NOT NULL,
# MAGIC   C_NAME STRING NOT NULL,
# MAGIC   C_ADDRESS STRING,
# MAGIC   C_NATIONKEY INT,
# MAGIC   C_PHONE STRING,
# MAGIC   C_ACCTBAL Double,
# MAGIC   C_MKTSEGMENT STRING,
# MAGIC   C_COMMENT STRING
# MAGIC ) USING DELTA;
# MAGIC 
# MAGIC -- 

# COMMAND ----------

df_cust = spark.read.format("csv").option("header",True).option("inferSchema",True).load("/mnt/Gen2Source/Customer/CustomerwithNullCName.csv")

# COMMAND ----------

df_cust.count()

# COMMAND ----------

df_cust.write.format("delta").mode("append").saveAsTable("customer")

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Dropping NOT NULL contraint
# MAGIC ALTER TABLE customer CHANGE COLUMN C_NAME DROP NOT NULL;

# COMMAND ----------

# Re-running the same codeto load into customer delta table
df_cust.write.format("delta").mode("append").saveAsTable("customer")

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT * FROM customer

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Ensure you delete rows in the delta which has NULL value for C_NAME
# MAGIC ALTER TABLE customer CHANGE COLUMN C_NAME SET NOT NULL;

# COMMAND ----------



# COMMAND ----------

# MAGIC %sql
# MAGIC DROP TABLE IF EXISTS customer;
# MAGIC CREATE TABLE customer(
# MAGIC   C_CUSTKEY INT,
# MAGIC   C_NAME STRING ,
# MAGIC   C_ADDRESS STRING,
# MAGIC   C_NATIONKEY INT,
# MAGIC   C_PHONE STRING,
# MAGIC   C_ACCTBAL DOUBLE,
# MAGIC   C_MKTSEGMENT STRING,
# MAGIC   C_COMMENT STRING
# MAGIC ) USING DELTA;
# MAGIC ALTER TABLE customer ADD CONSTRAINT MKTSEGMENT CHECK (C_MKTSEGMENT = 'BUILDING');

# COMMAND ----------


# Running the same code to load into customer delta table. df-Cust has only one row for BUILDING for C_MKTSEGMENT
df_cust.write.format("delta").mode("append").saveAsTable("customer")

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Properties column of DESCRIBE output has the details about constraints
# MAGIC DESCRIBE DETAIL customer;

# COMMAND ----------

# MAGIC %sql
# MAGIC ALTER TABLE customer DROP CONSTRAINT MKTSEGMENT;

# COMMAND ----------

# Below code should succeed
df_cust.write.format("delta").mode("append").saveAsTable("customer")
