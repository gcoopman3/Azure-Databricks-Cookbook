# Databricks notebook source
display(dbutils.fs.ls("/mnt/Gen2Source/Orders/parquetFiles/"))

# COMMAND ----------

from pyspark.sql.functions import count,year

ordersDF = spark.read.format("parquet").load("/mnt/Gen2Source/Orders/parquetFiles/").withColumn('O_OrderYear',year("O_ORDERDATE"))

# COMMAND ----------

display(ordersDF.limit(5))

# COMMAND ----------

import pyspark.sql.functions as f
display(ordersDF.filter("O_ORDERSTATUS = 'O'").groupBy("O_OrderYear","O_ORDERPRIORITY").agg(count("*").alias("TotalOrders")).orderBy("TotalOrders", ascending=False).limit(20))

# COMMAND ----------

# Saving the parquetFiles read in Delta format
ordersDF.write.format("delta").mode("overwrite").partitionBy("O_OrderYear").save("/mnt/Gen2Source/Orders/OrdersDelta/")

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Gen2Source/Orders/OrdersDelta/"))

# COMMAND ----------

# Checking the number of files created in 1993 folder. There are 9 files created
display(dbutils.fs.ls("dbfs:/mnt/Gen2Source/Orders/OrdersDelta/O_OrderYear=1993/"))

# COMMAND ----------

display(spark.sql("DROP TABLE  IF EXISTS Orders"))

display(spark.sql("CREATE TABLE Orders USING DELTA LOCATION '/mnt/Gen2Source/Orders/OrdersDelta'"))
                  
display(spark.sql("OPTIMIZE Orders ZORDER BY (O_ORDERPRIORITY)"))

# COMMAND ----------

# Lets re-execuete the same query used in cell 4
from pyspark.sql.functions import count
ordersDeltaDF = spark.read.format("Delta").load("/mnt/Gen2Source/Orders/OrdersDelta/")
display(ordersDeltaDF.filter("O_ORDERSTATUS = 'O'").groupBy("O_OrderYear","O_ORDERPRIORITY").agg(count("*").alias("TotalOrders")).orderBy("TotalOrders", ascending=False).limit(20))

# COMMAND ----------

display(dbutils.fs.ls("dbfs:/mnt/Gen2Source/Orders/OrdersDelta/O_OrderYear=1993/"))

# COMMAND ----------

# MAGIC %sql
# MAGIC VACUUM Orders;  -- Based on the delta table name. You can include the retention period. Delete files older than the mentioned hours.  VACUUM [ table_identifier | path] [RETAIN num HOURS]

# COMMAND ----------

# MAGIC %sql
# MAGIC VACUUM '/mnt/Gen2Source/Orders/OrdersDelta/' -- Based on the delta table files location
