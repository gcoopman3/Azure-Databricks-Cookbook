# Databricks notebook source
display(spark.sql("DESCRIBE HISTORY customerdelta"))

# COMMAND ----------

# Total rows currently CustomerDelta table = 2,850,000
df = spark.read \
  .format("delta") \
  .load("/mnt/Gen2Source/CustomerDelta/")
print(df.count())

# COMMAND ----------

# Let's read using timestampAsOf to count the number of rows
df = spark.read \
  .format("delta") \
  .option("timestampAsOf", "2021-08-08T11:10:55.000+0000") \
  .load("/mnt/Gen2Source/CustomerDelta/")

print(df.count())

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT * FROM CustomerDelta TIMESTAMP AS OF "2021-08-08T11:10:55.000+0000" limit 50;

# COMMAND ----------

df = spark.read \
  .format("delta") \
  .option("versionAsOf", "1") \
  .load("/mnt/Gen2Source/CustomerDelta/")
print(df.count())

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT count(*) FROM CustomerDelta VERSION AS OF 1 ;

# COMMAND ----------

# MAGIC %sql
# MAGIC INSERT INTO CustomerDelta
# MAGIC SELECT * FROM CustomerDelta TIMESTAMP AS OF "2021-04-10T19:08:54.000+0000"
# MAGIC WHERE C_MKTSEGMENT = = 'BUILDING'
