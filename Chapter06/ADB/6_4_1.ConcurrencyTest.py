# Databricks notebook source
# MAGIC %sql
# MAGIC DELETE FROM CustomerDelta WHERE C_MKTSEGMENT='FURNITURE'

# COMMAND ----------

# MAGIC %sql
# MAGIC DELETE FROM CustomerDeltaPartition WHERE C_MKTSEGMENT='FURNITURE'

# COMMAND ----------

# MAGIC %sql
# MAGIC DELETE FROM CustomerDeltaPartition WHERE C_MKTSEGMENT='MACHINERY' AND C_CUSTKEY=1377

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT * FROM CustomerDeltaPartition WHERE C_MKTSEGMENT='MACHINERY'
