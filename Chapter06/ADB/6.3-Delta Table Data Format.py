# Databricks notebook source
storageAccount="cookbookaadass2storage"
mountpoint = "/mnt/Gen2Source"
storageEndPoint ="abfss://rawdata@{}.dfs.core.windows.net/".format(storageAccount)
print ('Mount Point ='+mountpoint)

#ClientId, TenantId and Secret is for the Application(ADLSGen2App). You can use any SPN which has access to the storage
clientID =""
tenantID =""
clientSecret =""
oauth2Endpoint = "https://login.microsoftonline.com/{}/oauth2/token".format(tenantID)


configs = {"fs.azure.account.auth.type": "OAuth",
           "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
           "fs.azure.account.oauth2.client.id": clientID,
           "fs.azure.account.oauth2.client.secret": clientSecret,
           "fs.azure.account.oauth2.client.endpoint": oauth2Endpoint}
# dbutils.fs.mount(
#   source = storageEndPoint,
#   mount_point = mountpoint,
#   extra_configs = configs)
try:
  dbutils.fs.mount(
  source = storageEndPoint,
  mount_point = mountpoint,
  extra_configs = configs)
except:
    print("Already mounted...."+mountpoint)


# COMMAND ----------

display(dbutils.fs.ls("/mnt/Gen2Source/Customer/parquetFiles"))

# COMMAND ----------

db = "deltadb"
 
spark.sql(f"CREATE DATABASE IF NOT EXISTS {db}")
spark.sql(f"USE {db}")
 
spark.sql("SET spark.databricks.delta.formatCheck.enabled = false")
spark.sql("SET spark.databricks.delta.properties.defaults.autoOptimize.optimizeWrite = true")

# COMMAND ----------

import random
from datetime import datetime
from pyspark.sql.functions import *
from pyspark.sql.types import *
from delta.tables import *
from pyspark.sql.functions  import from_unixtime
from pyspark.sql.functions  import to_date
from pyspark.sql import Row
from pyspark.sql.functions import to_json, struct
from pyspark.sql import functions as F
import random
  

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading Customer parquet files from the mount point and writing to Delta tables.

# COMMAND ----------

#Reading parquet files and adding a new column to the dataframe and writing to delta table
cust_path = "/mnt/Gen2Source/Customer/parquetFiles"
 
df_cust = (spark.read.format("parquet").load(cust_path)
      .withColumn("timestamp", current_timestamp()))
 
df_cust.write.format("delta").mode("overwrite").save("/mnt/Gen2Source/Customer/delta")

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating Delta table 
# MAGIC DROP TABLE IF EXISTS Customer;
# MAGIC CREATE TABLE Customer
# MAGIC USING delta
# MAGIC location "/mnt/Gen2Source/Customer/delta"

# COMMAND ----------

# MAGIC %sql
# MAGIC describe formatted Customer

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from Customer limit 10;

# COMMAND ----------

# Reading new data which is in the folder parquetFiles_Dialy  in the /nnt/Gen2Source/Customer
#This is scenario where new data is coming on daily basis and no updates to existing. If we need to handle updates then you might have to use Merge as shown in the first recipe(Delta Table Operations-Create,Read,Write) of this chapter.

daily_cust_path = "/mnt/Gen2Source/Customer/parquetFiles_Daily"
 
df_cust_daily = (spark.read.format("parquet").load(daily_cust_path)
      .withColumn("timestamp", current_timestamp()))
 
df_cust_daily.write.format("delta").mode("append").save("/mnt/Gen2Source/Customer/delta")
 


# COMMAND ----------

# MAGIC %sql
# MAGIC -- Deleting from Delta table
# MAGIC DELETE FROM Customer WHERE C_CUSTKEY=82533

# COMMAND ----------

# MAGIC %sql
# MAGIC UPDATE Customer SET C_MKTSEGMENT="BUILDING" WHERE C_CUSTKEY=101275

# COMMAND ----------

# MAGIC %md
# MAGIC ### Writing Streaming Data into Delta Table and checking the delta log

# COMMAND ----------

#Creating the schema for the vehicle data json structure
jsonschema = StructType() \
.add("id", StringType()) \
.add("timestamp", TimestampType()) \
.add("rpm", IntegerType()) \
.add("speed", IntegerType()) \
.add("kms", IntegerType()) 

# COMMAND ----------

# MAGIC %fs mkdirs /mnt/Gen2Source/Vehicle_Delta/Chkpnt

# COMMAND ----------

def checkpoint_dir(): 
  return "/mnt/Gen2Source/Vehicle_Delta/Chkpnt/%s" % str(random.randint(0, 10000))

# COMMAND ----------

BOOTSTRAP_SERVERS = "kafkaenabledeventhubns.servicebus.windows.net:9093"
EH_SASL = "kafkashaded.org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://kafkaenabledeventhubns.servicebus.windows.net/;SharedAccessKeyName=sendreceivekafka;SharedAccessKey=zzzzzzzz\";"
GROUP_ID = "$Default"


# COMMAND ----------

# Function to read data from EventHub and writing as delta format
def append_kafkadata_stream(topic="eventhubsource1"):
  kafkaDF = (spark.readStream \
    .format("kafka") \
    .option("subscribe", topic) \
    .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
    .option("kafka.sasl.mechanism", "PLAIN") \
    .option("kafka.security.protocol", "SASL_SSL") \
    .option("kafka.sasl.jaas.config", EH_SASL) \
    .option("kafka.request.timeout.ms", "60000") \
    .option("kafka.session.timeout.ms", "60000") \
    .option("kafka.group.id", GROUP_ID) \
    .option("failOnDataLoss", "false") \
    .option("startingOffsets", "latest") \
    .load().withColumn("source", lit(topic)))
  
  newkafkaDF=kafkaDF.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)","source").withColumn('vehiclejson', from_json(col('value'),schema=jsonschema))
  kafkajsonDF=newkafkaDF.select("key","value","source", "vehiclejson.*")
  
  query=kafkajsonDF.selectExpr(
                  "id"	  \
                  ,"timestamp"	   \
                  ,"rpm"	\
                  ,"speed" \
                  ,"kms"
                  ,"source") \
            .writeStream.format("delta") \
            .outputMode("append") \
            .option("checkpointLocation",checkpoint_dir()) \
            .start("/mnt/Gen2Source/Vehicle_Delta/") 
 
  return query

# COMMAND ----------

query_source1 = append_kafkadata_stream(topic='eventhubsource1')

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating the table on delta location
# MAGIC CREATE DATABASE IF NOT EXISTS deltadb;
# MAGIC CREATE TABLE IF NOT EXISTS deltadb.VehicleDetails_Delta
# MAGIC USING DELTA
# MAGIC LOCATION "/mnt/Gen2Source/Vehicle_Delta/"
