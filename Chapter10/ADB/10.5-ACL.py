# Databricks notebook source
#Reading the files from ADLS Gen-2 using AAD authentication of the user.
display(dbutils.fs.ls("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/parquetFiles"))

# COMMAND ----------

display(dbutils.fs.ls("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/parquetFiles_Daily"))

# COMMAND ----------

display(dbutils.fs.ls("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/"))

# COMMAND ----------

dbutils.fs.mkdirs("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/newFolder")

# COMMAND ----------

display(dbutils.fs.ls("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/"))

# COMMAND ----------

display(dbutils.fs.ls("abfss://sourcedata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/parquetFiles"))

# COMMAND ----------

#To read data from the parquet file we need to grant read access to appuser. Do that from Storage Explorer and run the following code

# COMMAND ----------

#Reading parquet files and adding a new column to the dataframe and writing to delta table
configs = {
  "fs.azure.account.auth.type": "CustomAccessToken",
  "fs.azure.account.custom.token.provider.class": spark.conf.get("spark.databricks.passthrough.adls.gen2.tokenProviderClassName")
}

# Optionally, you can add <directory-name> to the source URI of your mount point.
dbutils.fs.mount(
  source = "abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/",
  mount_point = "/mnt/Customer",
  extra_configs = configs)

# COMMAND ----------

#Reading parquet files and writing as delta format
cust_path = "/mnt/Customer/parquetFiles"
df_cust = (spark.read.format("parquet").load(cust_path))
 
df_cust.write.format("delta").mode("overwrite").save("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/newFolder/delta")

# COMMAND ----------

display(dbutils.fs.ls("abfss://rawdata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/newFolder/delta"))

# COMMAND ----------

# dbutils.fs.unmount("/mnt/Customer")
