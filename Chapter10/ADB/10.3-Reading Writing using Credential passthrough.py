# Databricks notebook source
#Reading the files from ADLS Gen-2 using AAD authentication of the user.
display(dbutils.fs.ls("abfss://sourcedata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/parquetFiles"))

# COMMAND ----------

db = "tpchdb"
 
spark.sql(f"CREATE DATABASE IF NOT EXISTS {db}")
spark.sql(f"USE {db}")
 
spark.sql("SET spark.databricks.delta.formatCheck.enabled = false")
spark.sql("SET spark.databricks.delta.properties.defaults.autoOptimize.optimizeWrite = true")

# COMMAND ----------

import random
from datetime import datetime
from pyspark.sql.functions import *
from pyspark.sql.types import *
from delta.tables import *
  

# COMMAND ----------

#Reading parquet files from ADL Gen-2 Account by providing absolute path
cust_path = "abfss://sourcedata@cookbookadlsgen2storage1.dfs.core.windows.net/Customer/parquetFiles"
 
df_cust = (spark.read.format("parquet").load(cust_path)
      .withColumn("timestamp", current_timestamp()))

# COMMAND ----------

df_cust.show()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading Customer parquet files from the mount point and writing to Delta tables.

# COMMAND ----------

configs = {
  "fs.azure.account.auth.type": "CustomAccessToken",
  "fs.azure.account.custom.token.provider.class": spark.conf.get("spark.databricks.passthrough.adls.gen2.tokenProviderClassName")
}

dbutils.fs.mount(
  source = "abfss://sourcedata@cookbookadlsgen2storage1.dfs.core.windows.net",
  mount_point = "/mnt/Gen2Source",
  extra_configs = configs)

# COMMAND ----------

#Reading parquet files and adding a new column to the dataframe and writing to delta table
cust_path = "/mnt/Gen2Source/Customer/parquetFiles"
 
df_cust = (spark.read.format("parquet").load(cust_path)
      .withColumn("timestamp", current_timestamp()))
 
df_cust.write.format("delta").mode("overwrite").save("/mnt/Gen2Source/Customer/delta")

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Gen2Source/Customer/delta"))

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating Delta table 
# MAGIC DROP TABLE IF EXISTS Customer;
# MAGIC CREATE TABLE Customer
# MAGIC USING delta
# MAGIC location "/mnt/Gen2Source/Customer/delta"

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from Customer limit 10;

# COMMAND ----------

#dbutils.fs.unmount("/mnt/Gen2Source")
