# Databricks notebook source
storageAccount="cookbookadlsgen2storage1"
mountpoint = "/mnt/Gen2Source"
storageEndPoint ="abfss://rawdata@{}.dfs.core.windows.net/".format(storageAccount)
print ('Mount Point ='+mountpoint)

#ClientId, TenantId and Secret is for the Application(ADLSGen2App) was have created as part of this recipe
clientID ="xxxxxx-xx-xxxx-xxx-xxx"
tenantID ="xxxx-xxxx-xxxx-xxxx-xxxxxx"
clientSecret ="xxxxxx"
oauth2Endpoint = "https://login.microsoftonline.com/{}/oauth2/token".format(tenantID)


configs = {"fs.azure.account.auth.type": "OAuth",
           "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
           "fs.azure.account.oauth2.client.id": clientID,
           "fs.azure.account.oauth2.client.secret": clientSecret,
           "fs.azure.account.oauth2.client.endpoint": oauth2Endpoint}


dbutils.fs.mount(
source = storageEndPoint,
mount_point = mountpoint,
extra_configs = configs)





# COMMAND ----------

#dbutils.fs.unmount("/mnt/Gen2Source5")

# COMMAND ----------

display(dbutils.fs.ls("/mnt/Gen2Source/Customer/parquetFiles"))

# COMMAND ----------

import random
from datetime import datetime
from pyspark.sql.functions import *
from pyspark.sql.types import *
from delta.tables import *
  

# COMMAND ----------

# MAGIC %md
# MAGIC ### Reading Customer parquet files from the mount point and writing to Delta tables.

# COMMAND ----------

#Reading parquet files and adding a new column to the dataframe and writing to delta table
cust_path = "/mnt/Gen2Source/Customer/parquetFiles"
 
df_cust = (spark.read.format("parquet").load(cust_path)
      .withColumn("timestamp", current_timestamp()))
 
df_cust.write.format("delta").mode("overwrite").save("/mnt/Gen2Source/Customer/delta")

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating Delta table 
# MAGIC DROP TABLE IF EXISTS Customer;
# MAGIC CREATE TABLE Customer
# MAGIC USING delta
# MAGIC location "/mnt/Gen2Source/Customer/delta"

# COMMAND ----------

# MAGIC %sql
# MAGIC describe formatted Customer

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from Customer limit 10;
